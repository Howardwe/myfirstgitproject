
public class RecursiveUsingHelperMethods {
    /**
     * Given a string, compute recursively (no loops) the number of "11"
     * substring in the string. The "11" substrings should not overlap.
     * count11("11abc11") → 2 
     * count11("abc11x11x11") → 3 
     * count11("111") → 1
     */
    public static int count11(String str) {
        if (str.length() < 2) {
            return 0;
        }
        
        if (str.substring(0, 2).equals("11")) {
            return 1 + count11(str.substring(2));
        } else {
            return count11(str.substring(1));
        }
    }
    
    public static int count11_2(String str) {
        return count11Helper(str, 0);
    }
    
    public static int count11Helper(String str, int i) {
        if (i >= str.length() -1) {
            return 0;
        }
        
        if (str.charAt(i) == '1' && str.charAt(i + 1) == '1') {
            return 1 + count11Helper(str, i + 2);
        } else {
            return count11Helper(str, i + 1);
        }
    }
    
    
    /**
     * Given a string, return true if it is a nesting of zero or more pairs of
     * parenthesis, like "(())" or "((()))". 
     * Suggestion: check the first and 
     * last chars, and then recur on what's inside them. 
     * nestParen("(())") → true 
     * nestParen("((()))") → true 
     * nestParen("(((x))") → false
     */
    public static boolean nestParen(String str) {
        if (str.isEmpty()) {
            return true;
        }
        if (str.substring(0, 1).equals("(") &&
            str.substring(str.length() - 1).equals(")")) {
            return nestParen(str.substring(1, str.length() - 1));
        } else {
            return false;
        }
            
    }
    
    public static boolean nestParen_2(String str) {
        return nestParenHelper(str, 0, str.length() - 1);
    }
    
    public static boolean nestParenHelper(String str, int begin, int end) {
        if (begin > end) {
            return true;
        }
        
        if(str.charAt(begin) == '(' && str.charAt(end) == ')') {
            return nestParenHelper(str, begin + 1, end - 1);
        } else {
            return false;
        }
    }
    
    public static void main(String[] args) {
        System.out.println(count11("11abc11"));
        System.out.println(count11("abc11x11x11"));
        System.out.println(count11("111"));
        
        System.out.println("-".repeat(10));
        System.out.println(count11_2("11abc11"));
        System.out.println(count11_2("abc11x11x11"));
        System.out.println(count11_2("111"));
        
        System.out.println("-".repeat(10));
        System.out.println(nestParen("(())"));
        System.out.println(nestParen("((()))"));
        System.out.println(nestParen("(((x))"));
        
        System.out.println("-".repeat(10));
        System.out.println(nestParen_2("(())"));
        System.out.println(nestParen_2("((()))"));
        System.out.println(nestParen_2("(((x))"));
        
    }
}
